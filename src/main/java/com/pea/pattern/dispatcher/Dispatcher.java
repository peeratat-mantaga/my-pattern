package com.pea.pattern.dispatcher;

public class Dispatcher {
	
	private HomeView homeView;
	private CustomerView customerView;
	
	public Dispatcher() {
		super();
		this.homeView = new HomeView();
		this.customerView = new CustomerView();
	}
	
	public void dispatchRequest(String request) {
		if(request.equals("/home")) {
			this.homeView.show();
		}else if(request.equals("/customer")) {
			this.customerView.show();
		}else {
			System.out.println("Page Not Found.");
		}
	}
}
