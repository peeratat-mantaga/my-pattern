package com.pea.pattern.dao;

import java.util.List;

public interface StudentDAO {
	public List<Student> getStudents();
	public Student getStudent(int studentId);
	public void deleteStudent(int studentId);
	public void updateStudent(Student student);
}
