package com.pea.pattern.dao;

import java.util.ArrayList;
import java.util.List;

public class StudentDAOImpl implements StudentDAO {

	// data from database
	private List<Student> students;

	public StudentDAOImpl() {
		super();
		ArrayList<Student> students = new ArrayList<>();
		students.add(new Student("peeratat", "manataga", 0));
		students.add(new Student("chad", "darby", 1));
		students.add(new Student("mary", "public", 2));
		students.add(new Student("kaki", "koki", 3));
		this.students = students;
	}

	@Override
	public List<Student> getStudents() {
		return this.students;
	}

	@Override
	public Student getStudent(int studentId) {
		for (Student temp : this.students) {
			if (temp.getId() == studentId) return temp;
		}
		return null;
	}

	@Override
	public void deleteStudent(int StudentId) {
		for (Student temp : this.students) {
			if (temp.getId() == StudentId)
				this.students.remove(temp);
		}
	}

	@Override
	public void updateStudent(Student student) {

		for (int i = 0; i < this.students.size(); i++) {
			if (student.getId() == this.students.get(i).getId()) {
				Student temp = this.students.get(i);
				temp.setFirstName(student.getFirstName());
				temp.setLastName(student.getLastName());
			}
		}
	}

}
