package com.pea.pattern.app;

import java.util.ArrayList;

import com.pea.pattern.filter.FemaleCriteria;
import com.pea.pattern.filter.MaleCriteria;
import com.pea.pattern.filter.OrCriteria;
import com.pea.pattern.filter.Person;
import com.pea.pattern.filter.PersonCriteria;

public class FilterApp {

	public static void main(String[] args) {
		
		PersonCriteria femaleFilter = new FemaleCriteria();
		PersonCriteria maleFilter = new MaleCriteria();
		ArrayList<PersonCriteria> sumCriteria = new ArrayList<PersonCriteria>();
		sumCriteria.add(maleFilter);
		sumCriteria.add(femaleFilter);
		PersonCriteria sumFilter = new OrCriteria(sumCriteria);
		
		ArrayList<Person> people = new ArrayList<Person>();
		people.add(new Person("pee", "male"));
		people.add(new Person("pop","female"));
		people.add(new Person("pao", "other"));
		people.add(new Person("pap", "other"));
		people.add(new Person("jab", "male"));
		people.add(new Person("golf", "male"));
		people.add(new Person("pond", "female"));
		
		System.out.println("\nNo filter :\n" + people);
		System.out.println("\nFemale filer :\n" + femaleFilter.filter(people));
		System.out.println("\nMale filter :\n" + maleFilter.filter(people));
		System.out.println("\nMale or Female filter :\n" + sumFilter.filter(people));
	}

}
