package com.pea.pattern.app;

import com.pea.pattern.dao.Student;
import com.pea.pattern.dao.StudentDAO;
import com.pea.pattern.dao.StudentDAOImpl;

public class DaoApp {

	public static void main(String[] args) {
		StudentDAO studentDao = new StudentDAOImpl();
		
		System.out.println("\n//Get all students");
		System.out.println(studentDao.getStudents());
		
		System.out.println("\n//Get student : ID = 2");
		System.out.println(studentDao.getStudent(2));

		System.out.println("\n//Update Student(ID = 2) and get all");
		studentDao.updateStudent(new Student("poop", "poop", 2));
		System.out.println(studentDao.getStudents());

		System.out.println("\n//Delete Student(ID = 2) and get all");
		studentDao.deleteStudent(2);
		System.out.println(studentDao.getStudents());

	}

}
