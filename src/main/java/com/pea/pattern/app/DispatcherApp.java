package com.pea.pattern.app;

import com.pea.pattern.dispatcher.Dispatcher;

public class DispatcherApp {

	public static void main(String[] args) {
		Dispatcher dis = new Dispatcher();
		
		dis.dispatchRequest("/home");
		
		dis.dispatchRequest("/customer");
		
		dis.dispatchRequest("/student");

	}

}
