package com.pea.pattern.filter;

import java.util.List;

public interface PersonCriteria {
	public List<Person> filter(List<Person> people);
}
