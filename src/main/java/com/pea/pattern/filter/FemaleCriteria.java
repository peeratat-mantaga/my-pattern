package com.pea.pattern.filter;

import java.util.ArrayList;
import java.util.List;

public class FemaleCriteria implements PersonCriteria {

	@Override
	public List<Person> filter(List<Person> people) {
		ArrayList<Person> newPeople = new ArrayList<>();
		for(Person temp : people) {
			if(temp.getGender().equalsIgnoreCase("female")){
				newPeople.add(temp);
			}
		}
		return newPeople;
	}

}
