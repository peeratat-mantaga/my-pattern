package com.pea.pattern.filter;

import java.util.ArrayList;
import java.util.List;

public class OrCriteria implements PersonCriteria{

	private List<PersonCriteria> criteria;
	
	public OrCriteria(List<PersonCriteria> criteria) {
		super();
		this.criteria = criteria;
	}

	@Override
	public List<Person> filter(List<Person> people) {
		ArrayList<Person> newPeople = new ArrayList<>();
		
		for(PersonCriteria temp : this.criteria) {
			newPeople.addAll(temp.filter(people)); 
		}
		
		return newPeople;
	}
	
}
